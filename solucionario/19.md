## 19.  Alias (encabezados de columnas)

## Ejercicios 01

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

1. Elimine la tabla:

```sql
drop table articulos;
```

2. Cree la tabla:

```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);
```

3. Ingrese algunos registros:

```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```

4. El comercio hace un descuento del 15% en ventas mayoristas. Necesitamos recuperar el código, nombre, decripción de todos los artículos con una columna extra que muestre el precio de cada artículo para la venta mayorista con el siguiente encabezado "precio mayorista"
```sql
select codigo, nombre, descripcion, precio * 0.85 as "precio mayorista"
from articulos;
```
resultado:
```sql
101	impresora	Epson Stylus C45	340,68
203	impresora	Epson Stylus C85	425
205	monitor	Samsung 14	680
300	teclado	ingles Biswal	85
```
5. Muestre los precios de todos los artículos, concatenando el nombre y la descripción con el encabezado "artículo" (sin emplear "as" ni comillas)
```sql
artículo impresora Epson Stylus C45
artículo impresora Epson Stylus C85
artículo monitor Samsung 14
artículo teclado ingles Biswal

```
6. Muestre todos los campos de los artículos y un campo extra, con el encabezado "monto total" en la que calcule el monto total en dinero de cada artículo (precio por cantidad)
```sql
ORA-00923: palabra clave FROM no encontrada donde se esperaba
00923. 00000 -  "FROM keyword not found where expected"
*Cause:    
*Action:
Error en la línea: 31, columna: 9
```
7. Muestre la descripción de todas las impresoras junto al precio con un 20% de recargo con un encabezado que lo especifique.
```sql
Epson Stylus C45	480,96
Epson Stylus C85	600

```