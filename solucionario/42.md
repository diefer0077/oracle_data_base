# 42. Restricciones: eliminación (alter table - drop constraint)

## Ejercicios 001

Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".

1. Setee el formato de "date" para que nos muestre hora y minutos:

```sql
alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```

2. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);
```

3. Establezca una restricción "check" que admita solamente los valores "a" y "m" para el campo "tipo":

```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));
```

4. Agregue una restricción "primary key" que incluya los campos "patente" y "horallegada"
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);
```

5. Ingrese un vehículo.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'a', SYSDATE);
```

6. Intente ingresar un registro repitiendo la clave primaria.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'a', SYSDATE);
-- Esto generará un error debido a la violación de la restricción de clave primaria.
```

7. Ingrese un registro repitiendo la patente pero no la hora de llegada.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'm', SYSDATE);
-- Esto se permitirá ya que la clave primaria no se está duplicando.
```

8. Ingrese un registro repitiendo la hora de llegada pero no la patente.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('XYZ789', 'a', SYSDATE);
-- Esto se permitirá ya que la clave primaria no se está duplicando.
```

9. Vea todas las restricciones para la tabla "vehiculos"
aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```


10. Elimine la restricción "primary key"
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT PK_vehiculos;
```


11. Vea si se ha eliminado.
Ahora aparecen 3 restricciones.
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```


12. Elimine la restricción de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints").
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT <nombre_de_la_restriccion>;
```

13. Vea si se han eliminado.
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
14. Vuelva a establecer la restricción "primary key" eliminada.
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);
```

15. La playa quiere incluir, para el campo "tipo", además de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (camión). No puede modificar la restricción, debe eliminarla y luego redefinirla con los 3 valores.
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT CK_vehiculos_tipo;

ALTER TABLE vehiculos
ADD CONSTRAINT CK_vehiculos_tipo CHECK (tipo IN ('a', 'm', 'c'));
```

16. Consulte "user_constraints" para ver si la condición de chequeo de la restricción "CK_vehiculos_tipo" se ha modificado.
```sql
SELECT search_condition
FROM user_constraints
WHERE constraint_name = 'CK_vehiculos_tipo';
```

