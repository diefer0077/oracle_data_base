# 61. Restricciones foreign key (acciones)

## Ejercicios 001

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias":

```sql
drop table clientes;
drop table provincias;
```

2. Créelas con las siguientes estructuras:

```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)    );
```

3. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
```

4. Establezca una restricción "foreign key" especificando la acción "set null" para eliminaciones.
```sql
alter table clientes
add constraint fk_clientes_provincias
foreign key (codigoprovincia)
references provincias(codigo)
on delete set null;
```

5. Elimine el registro con código 3, de "provincias" y consulte "clientes" para ver qué cambios ha realizado Oracle en los registros coincidentes
Todos los registros con "codigoprovincia" 3 han sido seteados a null.
```sql
delete from provincias where codigo = 3;
select * from clientes;
```
6. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones
```sql
select * from user_constraints where constraint_name = 'FK_CLIENTES_PROVINCIAS';
```

7. Intente modificar el registro con código 2, de "provincias"
```sql
update provincias set codigo = 5 where codigo = 2;
```

8. Elimine la restricción "foreign key" establecida sobre "clientes"
```sql
alter table clientes
drop constraint fk_clientes_provincias;
```

9. Establezca una restricción "foreign key" sobre "codigoprovincia" de "clientes" especificando la acción "cascade" para eliminaciones
```sql
alter table clientes
add constraint fk_clientes_provincias
foreign key (codigoprovincia)
references provincias(codigo)
on delete cascade;
```
10. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre las restricciones "foreign key" de la tabla "clientes"
```sql
select * from user_constraints where constraint_name = 'FK_CLIENTES_PROVINCIAS';
```

11. Elimine el registro con código 2, de "provincias"
```sql
delete from provincias where codigo = 2;
```

12. Verifique que el cambio se realizó en cascada, es decir, que se eliminó en la tabla "provincias" y todos los clientes de la provincia eliminada
```sql
select * from provincias;
select * from clientes;
```

13. Elimine la restricción "foreign key"
```sql
alter table clientes
drop constraint fk_clientes_provincias;
```

14. Establezca una restricción "foreign key" sin especificar acción para eliminaciones
```sql
alter table clientes
add constraint fk_clientes_provincias
foreign key (codigoprovincia)
references provincias(codigo);
```
15. Intente eliminar un registro de la tabla "provincias" cuyo código exista en "clientes"
```sql
delete from provincias where codigo in (select codigoprovincia from clientes);
```

16. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre la restricción "FK_CLIENTES_CODIGOPROVINCIA"
```sql
select * from user_constraints where constraint_name = 'FK_CLIENTES_PROVINCIAS';
```

17. Intente elimimar la tabla "provincias"
```sql
delete from provincias;

```
18. Elimine la restricción "foreign key"
```sql
alter table clientes
drop constraint fk_clientes_provincias;
```

19. Elimine la tabla "provincias"
```sql
drop table provincias;
```