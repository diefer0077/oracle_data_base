## 23. Ordenar registros (order by)

## Ejercicios 01

En una página web se guardan los siguientes datos de las visitas: nombre, mail, pais y fecha.

1. Elimine la tabla "visitas" y créela con la siguiente estructura:

```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);
```

2. Ingrese algunos registros:

```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina',to_date('2020/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@hotmail.com>','Chile',to_date('2020/02/13 11:08:10', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/02 14:12:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/06/17 20:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/02/08 20:05:40', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/06 18:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2019/10/05 23:00:00', 'yyyy/mm/dd hh24:mi:ss'));
```

3. Ordene los registros por fecha, en orden descendente.
```sql
select * from visitas
order by fecha desc;
```
4. Muestre el nombre del usuario, pais y el mes, ordenado por pais (ascendente) y el mes (descendente)
```sql
Juancito	Argentina	10
Juancito	Argentina	07
Juancito	Argentina	07
Ana Maria Lopez	Argentina	05
Gustavo Gonzalez	Chile	02
Fabiola Martinez	Mexico	06
Fabiola Martinez	Mexico	02
```
5. Muestre los mail, país, ordenado por país, de todos los que visitaron la página en octubre (4 registros)
```sql
MAIL                                | PAIS
------------------------------------|---------
<MartinezFabiola@hotmail.com>       | Mexico
<JuanJosePerez@hotmail.com>         | Argentina
<AnaMaria@hotmail.com>              | Argentina
<GustavoGGonzalez@hotmail.com>      | Chile
```

