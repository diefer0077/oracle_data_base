## [5. Recuperar algunos registros (where)]

## Ejercicio 01

Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

1. Elimine "agenda"
```sql
DROP TABLE genda;
```
2. Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

3. Visualice la estructura de la tabla "agenda" (4 campos)
```sql
describe agenda;
```
4. Ingrese los siguientes registros ("insert into"):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');
```

5. Seleccione todos los registros de la tabla (5 registros)
```sql
select * from agenda;
```
6. Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
```sql
select * from agenda where nombre = 'Marisa';
```
7. Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql
select nombre, domicilio from agenda where apellido = 'Lopez';
```
8. Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)

No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".
```sql
select nombre, domicilio from agenda where apellido = 'Lopez';
```
9. Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
```sql
select nombre from agenda where telefono = '4545454';
```

## Ejercicio 02

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

1. Elimine la tabla si existe.
```sql
drop table  articulos;
```
2. Cree la tabla "articulos" con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
```

3. Vea la estructura de la tabla:

```sql
 describe articulos;
```

4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);
```

5. Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
```sql
select * from articulos where nombre = 'impresora';
```
6. Muestre sólo el código, descripción y precio de los teclados (2 registros)
```sql
select codigo, descripcion, precio from articulos where nombre = 'teclado';
```
