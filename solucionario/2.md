# 2. Ingresar registros (insert into- select)

# Ejercicio 001

1. Elimine la tabla "agenda"
Si no existe, un mensaje indicará tal situación.
```sql
drop table agenda;
```
2. Creamos la tabla agenda.
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
3. Un mensaje indica que la tabla ha sido creada exitosamente.
 ```sql
 Table AGENDA creado.
```
4. Intente crearla nuevamente.
Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.
```sql
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
5. Ingrese los siguientes registros:
```sql
INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('Gómez', 'Juan', 'Calle Principal 123', '1234567890');

INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('Pérez', 'María', 'Avenida Central 456', '9876543210');

INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('López', 'Carlos', 'Plaza Mayor 789', '5555555555');

```
6. Seleccione todos los registros de la tabla.
```sql
Gómez	Juan	Calle Principal 123	1234567890
Pérez	María	Avenida Central 456	9876543210
López	Carlos	Plaza Mayor 789	5555555555
```
7. Elimine la tabla "agenda"
```sql
Table AGENDA borrado.
```

8. Intente eliminar la tabla nuevamente (aparece un mensaje de error)
```sql
Error que empieza en la línea: 1 del comando :
drop table AGENDA
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
```

### Ejercicio 002

Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.

1. Elimine la tabla "libros"
```sql
drop table LIBROS;
```
2. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
```sql
CREATE TABLE libros (
    titulo VARCHAR(20),
    autor VARCHAR(30),
    editorial VARCHAR(15)
);
```
3. Visualice las tablas existentes.
```sql
El aleph	Borges	Planeta
Martin Fierro	Jose Hernandez	Emece
Aprenda PHP	Mario Molina	Emece
```
4. Visualice la estructura de la tabla "libros"

Muestra los campos y los tipos de datos de la tabla "libros".
```sql
TITULO	VARCHAR2(20 BYTE)	Yes		1	
AUTOR	VARCHAR2(30 BYTE)	Yes		2	
EDITORIAL	VARCHAR2(15 BYTE)	Yes		3	
```

5. Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');
```
6. Muestre todos los registros (select) de "libros"
```sql
CREATE TABLE libros (
 
    titulo VARCHAR(20),
    autor VARCHAR(30),
    editorial VARCHAR(15)
);


insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

SELECT * FROM libros;
```






















