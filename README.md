# DIEGO FERNANDO MAMANI RODRIGUEZ

## EJERCICIOS SQL

[1. Crear tablas (create table - describe - all_tables - drop table)](solucionario/1.md)

[2. Ingresar registros (insert into- select)](solucionario/2.md)

[3. Tipos de datos](solucionario/3.md)

[4. Recuperar algunos campos (select)](solucionario/4.md)

[5. Recuperar algunos registros (where)](solucionario/5.md)

[6. Operadores relacionales](solucionario/6.md)

[7. Borrar registros (delete)](solucionario/7.md)

[8. Actualizar registros (update)](solucionario/8.md)

[9. Comentarios](solucionario/9.md)

[10. Valores nulos (null)](solucionario/10.md)

[11. Operadores relacionales (is null)](solucionario/11.md)

[12. Clave primaria (primary key)](solucionario/12.md)

[13. Vaciar la tabla (truncate table)](solucionario/13.md)

[14. Tipos de datos alfanuméricos](solucionario/14.md)

[15. Tipos de datos numéricos](solucionario/15.md)

[16. Ingresar algunos campos](solucionario/16.md)

[17. Valores por defecto (default)](solucionario/17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](solucionario/18.md)

[19.Alias (encabezados de columnas)](solucionario/19.md)

[20. Funciones string](solucionario/20.md)

[21. Funciones matemáticas](solucionario/21.md)

[22. Funciones de fechas y horas](solucionario/22.md)

[23. Ordenar registros (order by)](solucionario/23.md)

[24. Operadores lógicos (and - or - not)](solucionario/24.md)

[25. Otros operadores relacionales (between)](solucionario/25.md)

[26. Otros operadores relacionales (in)](solucionario/26.md)

[27. Búsqueda de patrones (like - not like)](solucionario/27.md)

[28. Contar registros (count)](solucionario/28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](solucionario/29.md)

[30. Agrupar registros (group by)](solucionario/30.md)

[31. Seleccionar grupos (Having)](solucionario/31.md)

[32. Registros duplicados (Distinct)](solucionario/32.md)

[33. Clave primaria compuesta](solucionario/33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/34.md)

[35. Alterar secuencia (alter sequence)](solucionario/35.md)

[36.  Integridad de datos](solucionario/36.md)

[37. Restricción primary key](solucionario/37.md)

[38. Restricción unique](solucionario/38.md)

[39. Restriccioncheck](solucionario/39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario/41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario/42.md)

[43. Indices](solucionario/43.md)

[44. Indices (Crear . Información)](solucionario/44.md)

[45. Indices (eliminar)](solucionario/45.md)

[46. Varias tablas (join)](solucionario/46.md)

[47. Combinación interna (join)](solucionario/47.md)

[48. Combinación externa izquierda (left join)](solucionario/48.md)

[49. Combinación externa derecha (right join)](solucionario/49.md)

[50. Combinación externa completa (full join)](solucionario/50.md)

[51. Combinaciones cruzadas (cross)](solucionario/51.md)

[52. Autocombinación](solucionario/52.md)

[53. Combinaciones y funciones de agrupamiento	](solucionario/53.md)

[54. Combinar más de 2 tablas](solucionario/54.md)

[55. Otros tipos de combinaciones](solucionario/55.md)

[56. Clave foránea](solucionario/56.md)

[57. Restricciones (foreign key)](solucionario/57.md)

[58. Restricciones foreign key en la misma tabla](solucionario/58.md)

[59. Restricciones foreign key (eliminación)](solucionario/59.md)

[60. Restricciones foreign key deshabilitar y validar](solucionario/60.md)

[61. Restricciones foreign key (acciones)](solucionario/61.md)

[62. Información de user_constraints](solucionario/62.md)

[63. Restricciones al crear la tabla](solucionario/63.md)

[64. Unión](solucionario/64.md)

[65. Intersección](solucionario/65.md)

[66. Minus](solucionario/66.md)

[67. Agregar campos (alter table-add)](solucionario/67.md)

[68. Modificar campos (alter table - modify)](solucionario/68.md)

[69. Eliminar campos (alter table - drop)](solucionario/69.md)

[70. Agregar campos y restricciones (alter table)](solucionario/70.md)

[71. Subconsultas](solucionario/71.md)

[72. Subconsultas como expresion](solucionario/72.md)

[73. Subconsultas con in](solucionario/73.md)

[74. Subconsultas any- some - all](solucionario/74.md)

[75. Subconsultas correlacionadas](solucionario/75.md)

[76. Exists y No Exists](solucionario/76.md)

[77. Subconsulta simil autocombinacion](solucionario/77.md)

[78. Subconsulta conupdate y delete](solucionario/78.md)

[79. Subconsulta e insert](solucionario/79.md)

[80. Crear tabla a partir de otra (create table-select)](solucionario/80.md)

[81. Vistas (create view)](solucionario/81.md)

[82. Vistas (información)](solucionario/82.md)

[83. Vistas eliminar (drop view)](solucionario/83.md)

[84. Vistas (modificar datos a través de ella)](solucionario/84.md)

[85. Vistas (with read only)](solucionario/85.md)

[86. Vistas modificar (create or replace view)](solucionario/86.md)

[87. Vistas (with check option)](solucionario/87.md)

[88. Vistas (otras consideraciones: force)](solucionario/88.md)

[89. Vistas materializadas (materialized view)](solucionario/89.md)

[90. Procedimientos almacenados](solucionario/90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](solucionario/91.md)

[92. Procedimientos Almacenados (eliminar)](solucionario/92.md)

[93. Procedimientos almacenados (parámetros de entrada)](solucionario/93.md)

[94. Procedimientos almacenados (variables)](solucionario/94.md)

[95. Procedimientos Almacenados (informacion)](solucionario/95.md)

[96. Funciones](solucionario/96.md)

[97. Control de flujo (if)](solucionario/97.md)

[98. Control de flujo (case)](solucionario/98.md)

[99. Control de flujo (loop)](solucionario/99.md)

[100. Control de flujo (for)](solucionario/100.md)

[101. Control de flujo (while loop)](solucionario/101.md)

[102. Disparador (trigger)](solucionario/102.md)

[103. Disparador (información)](solucionario/103.md)

[104. Disparador de inserción a nivel de sentencia](solucionario/104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario/105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](solucionario/106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario/107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](solucionario/108.md)

[109. Disparador de actualización - lista de campos (update trigger)](solucionario/109.md)

[110. Disparador de múltiples eventos](solucionario/110.md)

[111. Disparador (old y new)](solucionario/111.md)

[112. Disparador condiciones (when)](solucionario/112.md)

[113. Disparador de actualizacion - campos (updating)](solucionario/113.md)

[114. Disparadores (habilitar y deshabilitar)](solucionario/114.md)

[115. Disparador (eliminar)](solucionario/115.md)

[116. Errores definidos por el usuario en trigger](solucionario/116.md)

[117. Seguridad y acceso a Oracle](solucionario/117.md)

[118. Usuarios (crear)](solucionario/118.md)

[119. Permiso de conexión](solucionario/119.md)

[120. Privilegios del sistema (conceder)](solucionario/120.md)

[121. Privilegios del sistema (with admin option)](solucionario/121.md)

[122. Modelado de base de datos](solucionario/122.md)